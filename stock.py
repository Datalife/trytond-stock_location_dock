# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, DeactivableMixin
from trytond.pool import PoolMeta
from trytond.pyson import Eval


class Location(metaclass=PoolMeta):
    __name__ = 'stock.location'

    docks = fields.One2Many('stock.location.dock', 'location', 'Docks',
        states={'invisible': Eval('type') != 'warehouse'})

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['docks'] = None
        return super(Location, cls).copy(records, default=default)

    @classmethod
    def view_attributes(cls):
        return super(Location, cls).view_attributes() + [
            ('//page[@id="docks"]', 'states', {
                'invisible': Eval('type') != 'warehouse'})]


class LocationDock(ModelView, ModelSQL, DeactivableMixin):
    """Location dock"""
    __name__ = 'stock.location.dock'

    location = fields.Many2One('stock.location', 'Location',
        ondelete='CASCADE', required=True, select=True,
        domain=[('type', '=', 'warehouse')],
        states={'readonly': ~Eval('active')})
    name = fields.Char('Name', required=True,
        states={'readonly': ~Eval('active')})
    code = fields.Char('Code',
        states={'readonly': ~Eval('active')})


class DockMixin(object):
    """Adds dock field to Model"""
    __slots__ = ()

    dock = fields.Many2One('stock.location.dock', 'Dock', ondelete='RESTRICT',
        domain=[('location', '=', Eval('warehouse'))],
        states={'readonly': Eval('state') != 'draft'})

    @fields.depends('warehouse')
    def on_change_with_dock(self):
        if self.warehouse and len(self.warehouse.docks) == 1:
            return self.warehouse.docks[0].id
        return None


class ShipmentOut(DockMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'


class ShipmentOutReturn(DockMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.return'


class ShipmentIn(DockMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'


class ShipmentInReturn(DockMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.in.return'

    warehouse = fields.Function(fields.Many2One('stock.location', 'Warehouse'),
        'get_warehouse')

    def get_warehouse(self, name):
        if self.from_location:
            return self.from_location.warehouse.id
        return None


class ShipmentInternal(DockMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'

    warehouse = fields.Function(fields.Many2One('stock.location', 'Warehouse'),
        'get_warehouse')

    def get_warehouse(self, name):
        if self.from_location:
            return self.from_location.warehouse.id
        return None
